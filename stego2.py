import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import binascii
from subprocess import Popen, PIPE

with open('dicts/my_spanish', 'r') as fp:
    dict_es = []
    for w in fp.readlines():
        dict_es.append(w.replace('\n',''))

with open('dicts/my_english', 'r') as fp:
    dict_en = []
    for w in fp.readlines():
        dict_en.append(w.replace('\n',''))

def count_dictwords(message):
    count = 0
    for dicc in [dict_es, dict_en]:
        for word in dicc:
            count += message.count(word)
    return count

def show_figure_4x2(title, subplots, imgs):
    fig, axes = plt.subplots(nrows=2, ncols=4, figsize=(15,20))
    fig.suptitle(title)
    for ax, sub, im in zip(axes.flatten(), subplots, imgs):
        ax.set_title(sub)
        ax.imshow(im, cmap='gray', vmin=0, vmax=255)
    plt.show()

def byte2char(byte):
    char = binascii.unhexlify('%x' % (int('0b' + byte, 2)))
    return char.decode('utf-8')

def binary2string(binary):
    pointer = 0
    msg = ''
    while pointer < len(binary):
        byte = binary[pointer:pointer + 8]
        pointer = pointer + 8
        if byte == '00001010':
            msg += '\n'
        else:
            try:
                char = byte2char(byte)
                msg += char
            except Exception as e:
                #print("Error decoding byte " + byte + ": " + str(e))
                continue
    return msg

class StegoImage():
    def __init__(self, img):
        self.img = img
        # image shape as (Height, Width, Depth)
        self.H = self.img.shape[0]
        self.W = self.img.shape[1]
        if len(self.img.shape) > 2:
            self.D = self.img.shape[2]
        else:
            self.D = 1
        # Channels
        if self.D == 3:
            self.Channels = [StegoImage(self.img[:,:,0]), StegoImage(self.img[:,:,1]), StegoImage(self.img[:,:,2])]
        else:
            self.Channels = None

    def show(self, title=''):
        cv2.imshow(title, self.img)
        while cv2.getWindowProperty(title,cv2.WND_PROP_VISIBLE) > 0:
            k = cv2.waitKey(100)
            if k == 27 or k == ord('q'):
                cv2.destroyAllWindows()
                break

    def get_bitplans(self):
        imgs = [255 * ((self.img & (1<<i)) >>i) for i in range(8)]
        return imgs

    def get_square(self, xmin, ymin, xmax, ymax):
        square = self.img[ymin:ymax,xmin:xmax]
        return square

    def show_bitplans(self):
        imgs = self.get_bitplans()
        subplots = list(range(1,9))
        for idx, sub in enumerate(subplots):
            subplots[idx] = "Bit " + str(sub)
        show_figure_4x2('RGB Bit Plans', subplots, imgs)

    def show_bitplans_rgb(self):
        subplots = list(range(1,9))
        for idx, sub in enumerate(subplots):
            subplots[idx] = "Bit " + str(sub)

        for Channel,label in zip(self.Channels, ['R', 'G', 'B']):
            imgs = Channel.get_bitplans()
            show_figure_4x2(label + ' Bit Plans', subplots, imgs)

    def save_all_brightnesses(self, folder):
        grayImage = cv2.cvtColor(self.img, cv2.COLOR_RGB2GRAY)
        for b in range(256):
            img = np.zeros([self.H,self.W,self.D])
            for y in range(self.H):
                for x in range(self.W):
                    if grayImage[y,x] == b:
                        img[y,x] = 255
            filename = str(b) + '.jpg'
            filepath = os.path.join(folder, filename)
            cv2.imwrite(filepath, img)

    def bruteforce_LSB(self):
        self.bruteforce_horizontal_RGB()
        self.bruteforce_horizontal_RGB(reverse=True)
        self.bruteforce_vertical_RGB()
        self.bruteforce_vertical_RGB(reverse=True)

    def bruteforce_horizontal_RGB(self, max_jump=3, reverse=False):
        # jump = 3 is jumping to next pixel in same channel
        lsb = self.get_bitplans()[0]
        # 0 - R, 1 - G, 2 - B
        channel_orders = [[0, 1, 2], [0, 2, 1], [1, 0, 2], [1, 2, 0], [2, 0, 1], [2, 1, 0]]
        #channel_orders = [[2, 1, 0]]

        for channel_order in channel_orders:
            MY_LINES = []
            MY_JUMPS = []
            print("0 - R, 1 - G, 2 - B")
            print(channel_order)
            if not reverse:
                print("Reading left to right...")
            else:
                print("Reading right to left...")

            for y in range(self.H):
                lst = []
                for x in range(self.W):
                    for c in channel_order:
                        lst.append([y,x,c])
                if reverse:
                    lst.reverse()

                for jump in range(1,max_jump+1):
                    binary = ''
                    for v in range(0,len(lst),jump):
                        if lsb[lst[v][0],lst[v][1],lst[v][2]] > 0:
                            binary += '1'
                        else:
                            binary += '0'
                    msg = binary2string(binary)
                    count = count_dictwords(msg)
                    if count > 0:
                        MY_LINES.append(y)
                        MY_JUMPS.append(jump)
                        print(msg)
            if MY_LINES:
                print("Found info in lines: ")
                print(MY_LINES)
                print("Jumping pixels: ")
                print(MY_JUMPS)

    def bruteforce_vertical_RGB(self, max_jump=3, reverse=False):
        # jump = 3 is jumping to next pixel in same channel
        lsb = self.get_bitplans()[0]
        # 0 - R, 1 - G, 2 - B
        channel_orders = [[0, 1, 2], [0, 2, 1], [1, 0, 2], [1, 2, 0], [2, 0, 1], [2, 1, 0]]
        #channel_orders = [[2, 1, 0]]

        for channel_order in channel_orders:
            MY_COLUMNS = []
            MY_JUMPS = []
            print("0 - R, 1 - G, 2 - B")
            print(channel_order)
            if not reverse:
                print("Reading up to down...")
            else:
                print("Reading down to up...")

            for x in range(self.W):
                lst = []
                for y in range(self.H):
                    for c in channel_order:
                        lst.append([y,x,c])
                if reverse:
                    lst.reverse()

                for jump in range(1,max_jump+1):
                    binary = ''
                    for v in range(0,len(lst),jump):
                        if lsb[lst[v][0],lst[v][1],lst[v][2]] > 0:
                            binary += '1'
                        else:
                            binary += '0'
                    msg = binary2string(binary)
                    count = count_dictwords(msg)
                    if count > 0:
                        MY_COLUMNS.append(x)
                        MY_JUMPS.append(jump)
                        print(msg)
            if MY_COLUMNS:
                print("Found info in columns: ")
                print(MY_COLUMNS)
                print("Jumping pixels: ")
                print(MY_JUMPS)


img = cv2.imread('LenaLSB.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
I = StegoImage(img)

# Show bitplans
#I.show_bitplans()
#I.show_bitplans_rgb()

# Save all brightnesses
#I.save_all_brightnesses('/tmp')

I.bruteforce_LSB()
